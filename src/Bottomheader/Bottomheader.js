import React from 'react';
import './Bottomheader.css';

const Bottomheader = () => {
  return (
    <div className='Bottomheader'>
        <div>
          Reference  {">"}  HTML
        </div>

        <div>
          English (us)
        </div>
    </div>
  );
}

export default Bottomheader;
