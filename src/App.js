import logo from './logo.svg';
import './App.css';
import Topnavbar from './Topnavbar/Topnavbar.js';
import Header from './Header/Hearder.js';
import Bottomheader from './Bottomheader/Bottomheader.js';
import Sectionbody from './Sectionbody/Sectionbody.js';

function App() {
  return (
    <div className="App">
      <Topnavbar></Topnavbar>
      <Header></Header>
      <Bottomheader></Bottomheader>
      <Sectionbody></Sectionbody>

    </div>
  );
}

export default App;
