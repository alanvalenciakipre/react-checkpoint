import React from 'react';
import './Topnavbar.css';

const Topnavbar = () => {
  return (
    <div className='Topnavbar'>
      <div>
        <a href=""><span>mdn</span> <span> plus </span></a>
        now available in
        <span class="span-nav"> </span>
        country! Support MDN
        <span> and </span>
        make it your own.
        <a href=""><span>Learn more</span></a>
      </div>
    </div>
  );
}

export default Topnavbar;
