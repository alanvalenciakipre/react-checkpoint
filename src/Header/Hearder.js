import React from 'react';
import './Header.css'

const Hearder = () => {
  return (
    <div className='Header'>
      <div>
        <ul>
          <li><a href="">mdn web docs</a></li>
          <li><a href="">References</a></li>
          <li><a href="">Guides</a></li>
          <li><a href="">MDN Plus</a></li>

        </ul>

        <ul>
        <li><a href="">Theme</a></li>
          <li><a href="">Already a subscriber?</a></li>
          <input type="text" />
          <li><a href="">Get MDN Plus</a></li>
        </ul>
      </div>
    </div>
  );
}

export default Hearder;
