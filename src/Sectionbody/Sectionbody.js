import React from 'react';
import './Sectionbody.css';

const Sectionbody = () => {
  return (
    <div >
      <div className='Sectionbody'>
        <aside>
          <h2>Related Topics</h2>
          <h3>Tutorials:</h3>
          <ul>
            <li>Notions élémentaires de HTML</li>
            <li>Introduction à HTML</li>
            <li>Multimédia et intégration</li>
            <li>Tableaux HTML</li>
          </ul>
          <h3>Références:</h3>
          <ul>
            <li>Element HTML</li>
            <li>Attributs universels</li>
            <li>Types input </li>
          </ul>
          <h3>Documentation:</h3>
          <li>Listes utiles</li>
          <li>Contribuer</li>
        </aside>
        <div>
          <h1>HTML (HyperText Markup Language)</h1>
          <p><span>HTML</span> signifie « HyperText Markup Language » qu'on peut traduire par « langage de balises pour l'hypertexte ». Il est utilisé
            afin de créer et de représenter le contenu d'une page web et sa structure. D'autres technologies sont utilisées avec
            HTML pour décrire la présentation d'une page <span>(CSS)</span> et/ou ses fonctionnalités interactives <span> (JavaScript). </span></p>
          <p>L'« hypertexte » désigne les liens qui relient les pages web entre elles, que ce soit au sein d'un même site web ou
            entre différents sites web. Les liens sont un aspect fondamental du Web. Ce sont eux qui forment cette « toile » (ce mot
            est traduit par web en anglais). En téléchargeant du contenu sur l'Internet et en le reliant à des pages créées par
            d'autres personnes, vous devenez un participant actif du World Wide Web.</p>
          <p>Le langage HTML utilise des « balises » pour annoter du texte, des images et d'autres contenus afin de les afficher dans
            un navigateur web. Le balisage HTML comprend des « éléments » spéciaux tels que </p>
        </div>
        <aside>
          <h3>In this article</h3>
          <ul>
            <li>Ressources clés</li>
            <li>Tutoriels pour les éléments</li>
            <li>Sujets avancés</li>
            <li>Références</li>
            <li>Sujets connexes</li>
          </ul>
        </aside>
      </div>


    </div>
  );
}

export default Sectionbody;
